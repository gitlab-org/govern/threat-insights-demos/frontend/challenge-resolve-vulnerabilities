// From https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS 
// Username: ^(([a-z])+.)+[A-Z]([a-z])+$
// Password: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa! 

String userName = textBox1.Text;
String password = textBox2.Text;
Regex testPassword = new Regex(userName);
Match match = testPassword.Match(password);
if (match.Success)
{
    MessageBox.Show("Do not include name in password.");
}
else
{
    MessageBox.Show("Good password.");
}