# Import dependencies
import hashlib

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def set_password(self, password):
        self.password = hashlib.md5(password.encode()).hexdigest()

# test class

def test_user():
    # Create a test user
    user = User("testuser", "password123")
    
    # Test initial password
    assert user.password == "password123"
    
    # Test password hashing
    user.set_password("newpassword")
    hashed_password = hashlib.md5("newpassword".encode()).hexdigest()
    assert user.password == hashed_password
    
    print("All tests passed!")

# Run the test
test_user()
