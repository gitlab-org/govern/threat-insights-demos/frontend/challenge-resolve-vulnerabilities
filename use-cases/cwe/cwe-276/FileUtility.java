import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

public class FileUtility {
    public static void CreateFile(Path path) throws Exception {

        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null");
        }
        
        try {
            Files.createFile(path);
            System.out.println("File created successfully: " + path);
        } catch (FileAlreadyExistsException e) {
            System.out.println("File already exists: " + path);
        } catch (IOException e) {
            throw new Exception("Error creating file: " + e.getMessage(), e);
        }

        Set<PosixFilePermission> permsSet = new HashSet<>();
        permsSet.add(PosixFilePermission.OWNER_READ);
        permsSet.add(PosixFilePermission.OWNER_WRITE);
        permsSet.add(PosixFilePermission.OWNER_EXECUTE);
        permsSet.add(PosixFilePermission.GROUP_READ);
        permsSet.add(PosixFilePermission.GROUP_WRITE);
        permsSet.add(PosixFilePermission.GROUP_EXECUTE);
        permsSet.add(PosixFilePermission.OTHERS_READ);
        permsSet.add(PosixFilePermission.OTHERS_WRITE);
        permsSet.add(PosixFilePermission.OTHERS_EXECUTE);
        Files.setPosixFilePermissions(path, permsSet);
    }
    
}