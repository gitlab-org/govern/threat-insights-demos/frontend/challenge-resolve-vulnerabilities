import requests
from flask import Flask, request

app = Flask(__name__)

@app.route("/user")
def user_proxy():
    user_id = request.args["id"]

    resp = requests.get("https://internal.api.example.com/user/" + user_id)

    return resp.text

# Run 
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)