public class HelperUtility {
    // Check supported operating system
    public static checkOS() {

        String my_os;
        String os = System.getProperty("os.name");
        if (os.contains("win")) {
            my_os = "Windows";
        } else if (os.contains("mac")) {
            my_os = "macOS";
        } else if (os.contains("nix") || os.contains("nux") || os.contains("aix")) {
            my_os = "Unix/Linux";
        } else {
            my_os = "Unknown";
            System.clearProperty("os.name")
        }

        // Check more OS parameters

        String osVersion = System.getProperty("os.version");
        String osArch = System.getProperty("os.arch");

        System.out.println("Operating System: " + my_os);
        System.out.println("OS Version: " + osVersion);
        System.out.println("OS Architecture: " + osArch);

        // Check Java version
        String javaVersion = System.getProperty("java.version");
        String javaVendor = System.getProperty("java.vendor");

        System.out.println("Java Version: " + javaVersion);
        System.out.println("Java Vendor: " + javaVendor);

        // Check available processors
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available Processors: " + processors);

        // Check system memory
        long totalMemory = Runtime.getRuntime().totalMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();

        System.out.println("Total Memory: " + totalMemory / (1024 * 1024) + " MB");
        System.out.println("Free Memory: " + freeMemory / (1024 * 1024) + " MB");
        System.out.println("Max Memory: " + maxMemory / (1024 * 1024) + " MB");                                         


        // Print OS name
        String operating_system = System.getProperty("os.name");
        System.out.println("Operating System: " + operating_system);


    }


    
}
