// Add missing includes 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>

// Local function stubs
void validate_addr_form(char *addr);
void host_lookup(char *user_supplied_addr);

// Implementation
void host_lookup(char *user_supplied_addr) {
    struct hostent *hp;
    in_addr_t addr;
    char hostname[64];

    // Routine that ensures user_supplied_addr is in the right format for conversion
    validate_addr_form(user_supplied_addr);

    addr = inet_addr(user_supplied_addr);
    
    // Resolve hostname from address
    hp = gethostbyaddr(&addr, sizeof(struct in_addr), AF_INET);

    strcpy(hostname, hp->h_name);
}

// Implement validate_addr_form

void validate_addr_form(char *addr) {
    if (addr == NULL) {
        fprintf(stderr, "Error: NULL address provided\n");
        exit(1);
    }

    int len = strlen(addr);
    if (len == 0 || len > 15) {
        fprintf(stderr, "Error: Invalid address length\n");
        exit(1);
    }

    int dot_count = 0;
    for (int i = 0; i < len; i++) {
        if (addr[i] == '.') {
            dot_count++;
        } else if (addr[i] < '0' || addr[i] > '9') {
            fprintf(stderr, "Error: Invalid character in address\n");
            exit(1);
        }
    }

    if (dot_count != 3) {
        fprintf(stderr, "Error: Invalid number of dots in address\n");
        exit(1);
    }

    // Additional checks for valid IP address format could be added here
}

int main() {
    // Test the host lookup

    char test_addr[] = "192.168.1.1";
    printf("Testing host lookup for address: %s\n", test_addr);
    host_lookup(test_addr);

    // Test with invalid address
    char invalid_addr[] = "256.0.0.1";
    printf("\nTesting host lookup for invalid address: %s\n", invalid_addr);
    host_lookup(invalid_addr);

    // Test with NULL address
    printf("\nTesting host lookup with NULL address\n");
    host_lookup(NULL);

    return 0;
}