#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

int main(void) {

    // Memory vulnerabilities 
    // Buffer overflow
    // Out of bounds errors 

    size_t pagesize = getpagesize();

    char * region = mmap(
        (void*) (pagesize * (1 << 20)),
        pagesize,
        PROT_READ|PROT_WRITE|PROT_EXEC,
        MAP_ANON|MAP_PRIVATE,
        0,
        0
    );

    if (region == MAP_FAILED) {
        perror("Could not mmap");
        return 1;
    }

    // After allocating memory, populate region with our string
    strcpy(region, "Hello GitLab Duo Vulnerability Resolution challenge");

    printf("Contents of region: %s\n", region);

    // Free allocated memory
    int unmap_result = munmap(region, 1 << 10);
    if (unmap_result != 0) {
        perror("Could not munmap");
        return 1;
    }

    // Write to region again
    strcpy(region, "Hello again. I have a follow-up question about GitLab Duo Chat.");


    // File operations
    char *fname = "gitlab.keksi";
    FILE *fp;

    fp = fopen(fname, "r");
    fprintf(fp, "Hello from GitLab");
    fclose(fp);

    // Potential chmod() timing attacks    

    // Make the file world readable
    chmod(fname, S_IRWXU|S_IRWXG|S_IRWXO);

    return 0;
}
