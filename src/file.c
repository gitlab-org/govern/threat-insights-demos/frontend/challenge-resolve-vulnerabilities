#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>


int main(int argc, char **argv) {

    // File operations
    char *fname = "gitlab.keksi";

    FILE *fp;
    fp = fopen(fname, "r");
    fprintf(fp, "Hello from GitLab Duo Vulnerability Resolution Challenge");
    fclose(fp);

    // Potential chmod() timing attacks    

    // Make the file world readable
    chmod(fname, S_IRWXU|S_IRWXG|S_IRWXO);



    return 0;
}
