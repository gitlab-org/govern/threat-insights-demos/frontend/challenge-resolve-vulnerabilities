#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char **argv) {

    // Command operations

    // Print usage

    printf("Usage: %s <input> <command>\n", argv[0]);


    // Print the first argument to greet the caller - https://owasp.org/www-community/attacks/Format_string_attack
    printf("Hello");
    printf(argv[1]);

    // Pass the second argument to the system call - https://owasp.org/www-community/attacks/Command_Injection
    char *cat = "cat ";
    char *command;
    size_t commandLength;

    commandLength = strlen(cat) + strlen(argv[1]) + 1;
    command = (char *) malloc(commandLength);
    strncpy(command, cat, commandLength);
    strncat(command, argv[2], (commandLength - strlen(cat)) );

    system(command);    

    return 0;
}
