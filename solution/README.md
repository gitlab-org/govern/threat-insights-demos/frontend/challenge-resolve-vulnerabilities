# Solutions

## Java

1. Hardcoded password - https://gitlab.com/gitlab-da/workshops/gitlab-summit-2024/gitlab-duo-coffee-chat-summit-2024/challenge-resolve-vulnerabilities/-/merge_requests/4 

## C

1. chmod - OK - https://gitlab.com/gitlab-da/workshops/gitlab-summit-2024/gitlab-duo-coffee-chat-summit-2024/challenge-resolve-vulnerabilities/-/merge_requests/3 
1. strcpy - Needs fixes with Code Suggestions https://gitlab.com/gitlab-da/workshops/gitlab-summit-2024/gitlab-duo-coffee-chat-summit-2024/challenge-resolve-vulnerabilities/-/merge_requests/2 

## PHP 

1. https://gitlab.com/gitlab-da/workshops/gitlab-summit-2024/gitlab-duo-coffee-chat-summit-2024/challenge-resolve-vulnerabilities/-/merge_requests/1 